var step1 = function(callback){
    console.log("Step 1")
    callback(null, "1");
}
var step2 = function(arg1, callback){
    console.log("Step 2")
    callback(null, "2");
}
var finalStep = function(err, result){
    if(err) {
        console.log("Error");
    } 
    else {
        console.log(result);
    }
}


const async = require("async");
async.waterfall([
    step1,
    step2
], finalStep);